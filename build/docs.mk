
DOXYGEN := doxygen


$(PROJECT_DIR)/docs/html : $(PROJECT_DIR)/docs/doxy.config $(HEADER_FILES) 
	@rm -rf $@
	sed "s/@PROJECT_NAME@/$(BASE_NAME)/g" < $< > $(addsuffix .tmp,$<)
	@$(DOXYGEN) $(addsuffix .tmp,$<)
	rm -f $(addsuffix .tmp,$<)

.PHONY: docs

docs : $(PROJECT_DIR)/docs/html
