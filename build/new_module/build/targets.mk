# this makefile fragment defines various standard targets
# ls_sources : lists all source files that will be used for compilation
# ls_objects : lists all object files that will be produced

.PHONY : ls_sources ls_objects ls_tests

ls_sources:
	@echo $(SOURCE_FILES)

ls_objects:
	@echo $(OBJECT_FILES)

ls_tests:
	@echo $(TESTS)

# these targets generate products
# tests:  makes tests
# sure :  runs tests
# examples : make examples
# binaries : make binaries
# modules : make all modules
# libs : make the libararies (usually just 1)
# all : makes all products and runs tests
.PHONY: tests sure modules all clean

tests : $(TESTS)

sure : $(addsuffix .passed,$(TESTS)) tests module
	@export FAILED_TESTS="$(shell find . -name \*.failed)" && \
        if [ "$${FAILED_TESTS}" = "" ]; then \
		echo All tests passed;\
         else\
	  echo Tests failed: $${FAILED_TESTS};\
          false; \
         fi;

module : $(BASE_LIB)

clean : 
	rm -rf $(BASE_LIB)
	rm -rf $(BASE_DIR)/obj
	rm -rf $(BASE_DIR)/tests

all : sure
