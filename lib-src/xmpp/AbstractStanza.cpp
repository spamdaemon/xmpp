#include <xmpp/AbstractStanza.h>

namespace xmpp {

  AbstractStanza::AbstractStanza(const ::std::string& sName, const ::std::string& sNamespace) throws()
  : _name(sName),_namespace(sNamespace)
  {}
  
  AbstractStanza::~AbstractStanza() throws()
  {}

  const char* AbstractStanza::name() const throws()
  { return _name.c_str(); }

  const char* AbstractStanza::ns() const throws()
  { return _namespace.c_str(); }
}

