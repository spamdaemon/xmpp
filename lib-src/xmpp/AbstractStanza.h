#ifndef _XMPP_ABSTRACTSTANZA_H
#define _XMPP_ABSTRACTSTANZA_H

#ifndef _XMPP_STANZA_H
#include <xmpp/Stanza.h>
#endif

#include <string>

namespace xmpp {

  class AbstractStanza : public Stanza {
  private:
    AbstractStanza(const AbstractStanza&);
    AbstractStanza&operator=(const AbstractStanza&);

    /**
     * Constructor 
     * @param sName the stanza's name
     * @param sNamespace the stanza's namespace
     */
  protected:
    AbstractStanza(const ::std::string& sName, const ::std::string& sNamespace) throws();

    /** Destructor */
    ~AbstractStanza() throws();

    /**
     * Get the stanza name. This is the simple tag name of the stanza.
     * @return the tag name corresponding to this stanza
     */
  public:
    const char* name() const throws();

    /**
     * Get the namespace name for this stanza.
     * @return the namespace in which stanza is defined
     */
  public:
    const char* ns() const throws();

    /** The name  */
  private:
    const ::std::string _name;

    /** The namespace */
  private:
    const ::std::string _namespace;
  };
}
#endif
