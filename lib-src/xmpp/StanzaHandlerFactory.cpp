#include <xmpp/StanzaHandlerFactory.h>

using namespace ::std;
using namespace ::timber;

namespace xmpp {
  namespace {

    struct StanzaHandlerFactoryImpl : public StanzaHandlerFactory {
      StanzaHandlerFactoryImpl() throws() {}
      ~StanzaHandlerFactoryImpl() throws() {}

      unique_ptr<StanzaHandler> createHandler (const string& name, const string& ns) const throws()
      {
	return unique_ptr<StanzaHandler>();
      }

     
    };

  }

  StanzaHandlerFactory::StanzaHandlerFactory() throws()
  {}

  StanzaHandlerFactory::~StanzaHandlerFactory() throws()
  {}

  Reference<StanzaHandlerFactory> StanzaHandlerFactory::createDefaultFactory() throws()
  {
    return new StanzaHandlerFactoryImpl();
  }
}
