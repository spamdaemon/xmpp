#ifndef _XMPP_STANZASTREAM_H
#define _XMPP_STANZASTREAM_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIMBER_W3C_XML_PARSER_H
#include <timber/w3c/xml/Parser.h>
#endif

#include <iosfwd>


namespace xmpp {
  class Stanza;
  class StanzaHandlerFactory;
  class StanzaHandler;

  /**
   * This is an XMPP stream which produces a stream of 
   * stanza tags.
   */
  class StanzaStream {
    StanzaStream(const StanzaStream&);
    StanzaStream&operator=(const StanzaStream&);

    /** A document handler for the initial tag */
  private:
    struct StreamHandler;

    /**
     * Create a new stanza stream.
     * @param stanzaFactory a stanza handler factory
     * @param exception if the stream is not the beginning of an xmpp session
     */
  public:
    StanzaStream (const ::timber::Reference<StanzaHandlerFactory>& stanzaFactory) throws (::std::exception);

    /**
     * Destructor
     */
  public:
    ~StanzaStream() throws();

    /**
     * Determine if this stream is still valid. A valid stream
     * will still accept parsed data.
     * @return true if this stream is valid 
     */
  public:
    bool isValid() const throws();

    /**
     * Determine if this stream has been properly opened. This means that the
     * initial stream tag has been parsed.
     * @return true if this stream has been initialized
     */
  public:
    bool isOpen() const throws();

    /**
     * Determine if this stream has been closed. This means that the
     * closing stream tag has been parsed.
     */
  public:
    bool isClosed() const throws();

    /**
     * Parse some more data.
     * @param buf a data buffer
     * @param n the number of bytes in the buffer
     * @return the number of stanzas recognized and parsed since the last call to parseData
     * @throws ::std::exception if an error has occurred
     */
  public:
    void parseData (const char* buf, size_t n) throws (::std::exception);

    /**
     * Get the next stanza.
     * @return the a stanza or null if none has been parsed.
     */
  public:
    ::timber::Pointer<Stanza> nextStanza () throws ();

    /** The xml parser */
  private:
    ::std::unique_ptr< ::timber::w3c::xml::Parser> _parser;

    /** The root tag handler */
  private:
    ::std::unique_ptr<StreamHandler> _streamHandler;
  };
}


#endif
