#ifndef _XMPP_STANZAHANDLER_H
#define _XMPP_STANZAHANDLER_H

#ifndef _TIMBER_W3C_XML_ELEMENTHANDLER_H
#include <timber/w3c/xml/ElementHandler.h>
#endif

#ifndef _XMPP_STANZA_H
#include <xmpp/Stanza.h>
#endif

namespace xmpp {

  
  /**
   * The StanzaHandler is is able to create instances of ElementHandlers
   * that can parse a stanza. 
   */
  class StanzaHandler : public ::timber::w3c::xml::ElementHandler {
    StanzaHandler(const StanzaHandler&);
    StanzaHandler&operator=(const StanzaHandler&);

    /**
     * Constructor
     */
  protected:
    StanzaHandler() throws();

    /**
     * Destructor 
     */
  public:
    virtual ~StanzaHandler() throws() = 0;

    /**
     * Get the stanza that was parsed.
     * @return the stanza that was parsed (can be 0)
     */
  public:
    virtual ::timber::Pointer<Stanza> getStanza() const throws() = 0;
  };
}


#endif
