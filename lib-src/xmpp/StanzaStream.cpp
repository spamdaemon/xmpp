#include <xmpp/StanzaStream.h>
#include <xmpp/Stanza.h>
#include <xmpp/StanzaHandler.h>
#include <xmpp/StanzaHandlerFactory.h>

#include <timber/w3c/xml/Parser.h>
#include <timber/w3c/xml/DocumentHandler.h>
#include <timber/logging.h>

#include <ios>
#include <cstring>
#include <queue>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::timber::w3c::xml;

namespace xmpp {
  namespace {
    
    static const char XMPP_NS[] = { "http://etherx.jabber.org/streams" };
    
    static Log getLog() { return Log("xmpp.StanzaStream"); }
  }
  
  /** The stream tag handler */
  struct StanzaStream::StreamHandler : public DocumentHandler {
    StreamHandler(const Reference<StanzaHandlerFactory>& stanzaFactory) throws()
    : _opened(false),_closed(false),
      _inComment(false),
      _level(0),
      _handlers(stanzaFactory)
    {}
    
    ~StreamHandler() throws() 
    {}

    /** Pop a stanza */
    Pointer<Stanza> nextStanza() throws()
    {
      if (_stanzas.empty()) {
	return Pointer<Stanza>();
      }
      else {
	Pointer<Stanza> res(_stanzas.front());
	_stanzas.pop();
	return res;
      }
    }

    /** Determine if this stream handler is valid */
    inline bool isOpen() const throws()
    { return _opened; }
    inline bool isClosed() const throws()
    { return _closed; }
    
    void notifyStartElement (const char* name, const char* ns, const char* prefix)
    {
      ++_level;

      // level=1: root tag
      if (_level==1) {
	if (strcmp(name,"stream")!=0 || strcmp(ns,XMPP_NS)!=0) {
	  throw ::std::runtime_error("Not a valid XMPP stream : "+string(name));
	}
	
	// don't mark as initialized just yet! This will be done in notifyElementContent()
	return;
      }

      // level=2: stanza
      if (_level==2) {
	_stanzaHandler = _handlers->createHandler(name,ns);
      }
      
      // level>=2: stanza content
      if (_stanzaHandler.get()!=0) {
	_stanzaHandler->notifyStartElement(name,ns,prefix);
      }
    }

    void notifyElementContent()
    {
      if (_stanzaHandler.get()!=0) {
	_stanzaHandler->notifyElementContent();
	return;
      }
      
      // cannot get here unless either a stream tag was found, or the 
      // the stream handler is already initialized
      if (_level==1) {
	_opened = true;
	LogEntry e(getLog());
	e.info() << "Opened stanza stream : "; 
	if (!_from.empty()) {
	  e << "[From : " << _from << "] ";
	}  
	if (!_id.empty()) {
	  e << "[ID : " << _id << "] ";
	}  
	if (!_to.empty()) {
	  e << "[To : " << _to << "] ";
	}  
	if (!_version.empty()) {
	  e << "[Version : " << _version << "] ";
	}  
	if (!_language.empty()) {
	  e << "[Language : " << _language << "] ";
	}  
	e << doLog;
      }
    }

    void notifyAttribute (const char* name, const char* ns, const char* prefix, const char* value)
    {
      if (_stanzaHandler.get()!=0) {
	_stanzaHandler->notifyAttribute(name,ns,prefix,value);
	return;
      }

      // if we've got the root tag, but not marked the stream as opened, then 
      // the attribute is a stream attribute
      if (_level==1) {
	if (ns==0 || strlen(ns)>0) {
	  return;
	}
	
	if (strcmp(name,"from")==0) {
	  _from = value;
	}
	else if (strcmp(name,"id")==0) {
	  _id = value;
	}
	else if (strcmp(name,"to")==0) {
	  _to = value;
	}
	else if (strcmp(name,"version")==0) {
	  _version = value;
	}
	else if (strcmp(name,"lang")==0) { // assume xml:lang
	  _language = value;
	}
      }
    }
      
    void notifyEndElement(const char* name, const char* ns, const char* prefix)
    {
      if (_stanzaHandler.get()!=0) {
	_stanzaHandler->notifyEndElement(name,ns,prefix);
      }

      
      if (_level==2 ) {
	if (_stanzaHandler.get()==0) {
	  LogEntry(getLog()).info() << "Unsupported stanza {"<<ns<<"}"<<name<<doLog;
	}
	else {
	  Pointer<Stanza> s = _stanzaHandler->getStanza();
	  if(s) {
	    _stanzas.push(s);
	  }
	  _stanzaHandler.reset(0);
	}
      }
      
      if (_level==1) {
	_closed = true;
      }

      --_level;
    }

    void notifyStartCData ()
    {
      if (_stanzaHandler.get()!=0) {
	_stanzaHandler->notifyStartCData();
      }
    }
    void notifyEndCData ()
    {
      if (_stanzaHandler.get()!=0) {
	_stanzaHandler->notifyStartCData();
      }
    }
    
    void notifyStartComment ()
    {
      _inComment  = true;
    }
    void notifyEndComment ()
    {
      _inComment  = false;
    }

    void notifyCharacterData (const char* txt, int count)
    {
      if (_inComment || count<=0) {
	return;
      }
      if (_stanzaHandler.get()!=0) {
	_stanzaHandler->notifyCharacterData(txt,count);
      }
    }
    void notifyIgnorableWhitespace(const char* ws)
    {
      if (_inComment) {
	return;
      }

      if (_stanzaHandler.get()!=0) {
	_stanzaHandler->notifyIgnorableWhitespace(ws);
      }
    }
    void notifyProcessingInstruction (const char* target, const char* data)
    {
      if (_stanzaHandler.get()!=0) {
	_stanzaHandler->notifyProcessingInstruction(target,data);
      }
    }

  private:
    bool _opened;
    bool _closed;
    
  private:
    bool _inComment;
    
    /** The current xml level */
  private:
    size_t _level;

    /** A vector of stanza read so far */
  private:
    ::std::queue< Reference<Stanza> > _stanzas;
    
    /** The attribute */
  public:
    string _from;
    string _id;
    string _to;
    string _version;
    string _language;

    /** The current stanza handler */
  private:
    ::std::unique_ptr<StanzaHandler> _stanzaHandler;

    /** The stanza handler factory */
  private:
    ::timber::Reference<StanzaHandlerFactory> _handlers;

  };

  StanzaStream::StanzaStream (const Reference<StanzaHandlerFactory>& stanzaFactory) throws (::std::exception)
  : _parser( ::timber::w3c::xml::Parser::create()),
    _streamHandler(new StreamHandler(stanzaFactory))
  {
  }

  StanzaStream::~StanzaStream() throws()
  {}

  bool StanzaStream::isValid() const throws()
  { return _parser.get()!=0; }
  
  bool StanzaStream::isOpen() const throws () 
  {
    return _parser.get()!=0 && _streamHandler->isOpen();
  }

  bool StanzaStream::isClosed() const throws () 
  {
    return _parser.get()!=0 && _streamHandler->isClosed();
  }
  
  void StanzaStream::parseData (const char* buf, size_t n) throws (::std::exception)
  {
    if (_parser.get()==0) {
      throw ::std::runtime_error("StanzaStream is no valid");
    }
    // parse the data
    try {
      _parser->parse(buf,n,*_streamHandler);

      // if the closing tag has been encountered, then 
      // finish parsing
      if (_streamHandler->isClosed()) {
	_parser->finishParsing(*_streamHandler);
	_parser.reset(0);
      }
    }
    catch (const ::std::exception&) {
      _parser.reset(0);
      throw;
    }
  }

  ::timber::Pointer<Stanza> StanzaStream::nextStanza () throws()
  {
    return _streamHandler->nextStanza();
  }



}
