#ifndef _XMPP_SESSIONPORT_H
#define _XMPP_SESSIONPORT_H


#ifndef _TIMBER_NET_SERVER_SESSIONPORT_H
#include <timber/net/server/SessionPort.h>
#endif

namespace xmpp {
  
  /**
   * The XMPP session port. This port establishes the XMPP connection
   * with an individual client.
   * For more information about XMPP, see 
   * <a href="http://www.ietf.org/rfc/rfc3920.txt">Extensible Messaging and Presence Protocol<a/>.
   */
  class SessionPort : public ::timber::net::server::SessionPort {
    SessionPort(const SessionPort&);
    SessionPort&operator=(const SessionPort&);

    /**
     * Constructor
     */
  protected:
    SessionPort() throws();

    /** Destructor */
  public:
    ~SessionPort() throws();

    /**
     * Create a new sessionport.
     */
  public:
    static ::timber::Reference<SessionPort> create() throws();
  };

}

#endif
