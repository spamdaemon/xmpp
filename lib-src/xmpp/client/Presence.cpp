#include <xmpp/client/Presence.h>
#include <xmpp/StanzaHandler.h>

using namespace ::std;

namespace xmpp {
  namespace client {

    namespace { 
      static const char PRESENCE_NAME[] = { "presence" };
      static const char PRESENCE_NAMESPACE[] = { "jabber:client" };
    }

    Presence::Presence() throws()
    {}
    
    Presence::~Presence() throws()
    {}

    const char* Presence::name() const throws()
    { return PRESENCE_NAME; }

    const char* Presence::ns() const throws()
    { return PRESENCE_NAMESPACE; }

    void Presence::write (::std::ostream& out) const
    {
    }

    unique_ptr<StanzaHandler> Presence::createHandler() throws()
    {
      return unique_ptr<StanzaHandler>();
    }
  }
}
