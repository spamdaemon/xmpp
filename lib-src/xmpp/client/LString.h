#ifndef _XMPP_CLIENT_LSTRING_H
#define _XMPP_CLIENT_LSTRING_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _XSD_ELEMENTPARSER_H
#include <xsd/ElementParser.h>
#endif

#include <string>

namespace xmpp {
  namespace client {

    /**
     * The LString is used to represent a string value
     * with a language attribute.
     */
    class LString {

      /**
       * Create a new LString.
       * @param s the string
       */
    public:
      LString() throws();

      /**
       * Create a new LString.
       * @param s the string
       * @param lang the language
       */
    public:
      LString(const ::std::string& str, const ::std::string& lang) throws();

      /**
       * Create a new LString.
       * @param s the string
       */
    public:
      LString(const ::std::string& str) throws();

      /**
       * Destructor
       */
    public:
      ~LString() throws();


      /**
       * The string value.
       * @return the value of this string
       */
    public:
      const ::std::string& value() const throws()
      { return _value; }

      /**
       * The string value.
       * @return the value of this string
       */
    public:
      ::std::string& value() throws()
      { return _value; }

      /**
       * The string value.
       * @return the value of this string
       */
    public:
      const ::std::string& language() const throws()
      { return _language; }

      /**
       * The string value.
       * @return the value of this string
       */
    public:
      ::std::string& language() throws()
      { return _language; }

      /**
       * Get the parser for an Lstring
       * @return a document handler for an LString
       * @throws ::std::exception if a handler could not be created
       */
    public:
      static ::std::unique_ptr< xsd::ElementParser<LString> > createParser() throws (::std::exception);
 
      /** The content */
    private:
      ::std::string _value;

      /** The language */
    private:
      ::std::string _language;
    };
  }
}

#endif
