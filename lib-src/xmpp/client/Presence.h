#ifndef _XMPP_CLIENT_PRESENCE_H
#define _XMPP_CLIENT_PRESENCE_H

#ifndef _XMPP_STANZA_H
#include <xmpp/Stanza.h>
#endif

#ifndef _XSD_SEQUENCE_H
#include <xsd/Sequence.h>
#endif

#ifndef _XSD_OPTIONAL_H
#include <xsd/Optional.h>
#endif

namespace xmpp {
  class StanzaHandler;
  namespace client {

    /**
     * The presence stanza.
     */
    class Presence : public Stanza {
      Presence&operator=(const Presence&);
      Presence(const Presence&);

      /** What presence should shown */
    public:
      enum Show {
	AWAY,
	CHAT,
	DND,
	XA
      };

      /** The presence status (a string) */
    public:
      typedef ::std::string Status;

      /** The priority is a number from -128 to 127 */
    public:
      typedef ::canopy::Int8 Priority;

      /**
       * The show tag  rfc 3921, 2.2.2.1
       */
    public:
      ::xsd::Optional<Show> _show;

      /** The presence status; rfc 3921, 2.2.2.2 */
    public:
      ::xsd::Sequence<Status> _status;

      /** The priority; rfc 3921, 2.2.2.3 */
    public:
      ::xsd::Optional<Priority> _priority;

      /** A sequence of other tags */
    public:
      ::xsd::Sequence< ::xsd::Element> _children;

      /**
       * Create a new Presence stanza.
       * @param id the stanza id
       * @param t the stanza type
       */
    public:
      Presence() throws();

      /** Destructor */
    public:
      ~Presence() throws();

      /**
       * Get a standard presence handler
       * @return a stanza handler
       */
    public:
      static ::std::unique_ptr<StanzaHandler> createHandler() throws();

      /**
       * @Override
       */
      const char* name() const throws();
      const char* ns() const throws();
      void write(::std::ostream&) const;

    };
  }
}
#endif
