#ifndef _XMPP_CLIENT_MESSAGE_H
#define _XMPP_CLIENT_MESSAGE_H

#ifndef _XMPP_STANZA_H
#include <xmpp/Stanza.h>
#endif

#ifndef _XMPP_CLIENT_LSTRING_H
#include <xmpp/client/LString.h>
#endif

#ifndef _XSD_SEQUENCE_H
#include <xsd/Sequence.h>
#endif

#ifndef _XSD_OPTIONAL_H
#include <xsd/Optional.h>
#endif

#ifndef _XSD_NMTOKEN_H
#include <xsd/NMToken.h>
#endif

namespace xmpp {
  class StanzaHandler;
  namespace client {

    /**
     * The message stanza.
     */
    class Message : public Stanza {
      Message&operator=(const Message&);
      Message(const Message&);

      /** The message subject is a just a simple string */
    public:
      typedef LString Subject;

      /** The message body */
    public:
      typedef LString Body;

      /** The message thread (actually, NMTOKEN) */
    public:
      typedef ::xsd::NMToken Thread;

      /** The type of Message stanza */
    public:
      enum Type { 
	CHAT,
	ERROR,
	GROUP_CHAT,
	HEADLINE,
	NORMAL
      };

      /** The bodies; see RFC 3921, 2.1.2.2 */
    public:
      ::xsd::Sequence<LString> body;

      /** The subjects; see RFC 3921, 2.1.2.2 */
    public:
      ::xsd::Sequence<LString> subject;

      /** The message thread */
    public:
      ::xsd::Optional<Thread> thread;

      /**
       * Create a new Message stanza.
       */
    public:
      Message() throws();

      /** Destructor */
    public:
      ~Message() throws();

      /**
       * Get a standard presence handler
       * @return a stanza handler
       */
    public:
      static ::std::unique_ptr<StanzaHandler> createHandler() throws();

      /**
       * @Override
       * @{
       */
      const char* name() const throws();
      const char* ns() const throws();
      void write(::std::ostream&) const;

      /*@}*/

    };
  }
}
#endif
