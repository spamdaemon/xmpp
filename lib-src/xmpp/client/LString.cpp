#include <xmpp/client/LString.h>
#include <cstring>

using namespace ::std;

namespace xmpp {
  namespace client {

    namespace {
      
      struct Handler : public xsd::ElementParser<LString> {
	Handler() 
	  : _level(0) {}
	~Handler() throws() {}

	void notifyStartElement (const char* name, const char* ns, const char* prefix)
	{
	  ++_level;
	  
	  // level=1: root tag
	  if (_level==1) {
	    element() = LString();
	  }
	  else {
	    throw ::std::runtime_error(string("Invalid child tag {")+ns+"}"+name);
	  }
	}
	
	void notifyAttribute (const char* name, const char* ns, const char* prefix, const char* value)
	{
	  if (strcmp(name,"lang")==0) { // assume xml:lang
	    element().language() = value;
	  }
	  else {
	    throw ::std::runtime_error(string("Invalid attribute {")+ns+"}"+name);
	  }
	  
	}
	
	void notifyEndElement(const char* name, const char* ns, const char* prefix)
	{
	  --_level;
	}
	
	void notifyCharacterData (const char* txt, size_t count)
	{
	  element().value().append(txt,count);
	}

      private:
	size_t _level;
      };
    }
    
    LString::LString() throws()
    {}

    LString::LString(const ::std::string& str, const ::std::string& lang) throws()
    : _value(str),_language(lang)
    {}

    LString::LString(const ::std::string& str) throws()
    : _value(str)
    {}

    LString::~LString() throws()
    {}

      ::std::unique_ptr< xsd::ElementParser<LString> > LString::createParser() throws (::std::exception)
    {
      return unique_ptr<xsd::ElementParser<LString> >(new Handler());
    }

    
  }
}
