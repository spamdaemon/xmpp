#ifndef _XMPP_CLIENT_IQ_H
#define _XMPP_CLIENT_IQ_H

#ifndef _XMPP_STANZA_H
#include <xmpp/Stanza.h>
#endif

namespace xmpp {
  class StanzaHandler;
  namespace client {

    /**
     * The Info/Queury stanza.
     */
    class IQ : public Stanza {
      IQ&operator=(const IQ&);
      IQ(const IQ&);

      /** The type of IQ stanza */
    public:
      enum Type { 
	GET,
	SET,
	RESULT,
	ERROR
      };

      /**
       * Create a new IQ stanza.
       * @param id the stanza id
       * @param t the stanza type
       */
    public:
      IQ() throws();

      /** Destructor */
    public:
      ~IQ() throws();

      /**
       * @Override
       */
      const char* name() const throws();
      const char* ns() const throws();
      void write(::std::ostream&) const;

      /*@}*/


      /**
       * Get a standard presence handler
       * @return a stanza handler
       */
    public:
      static ::std::unique_ptr<StanzaHandler> createHandler() throws();

    };
  }
}
#endif
