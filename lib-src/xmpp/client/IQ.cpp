#include <xmpp/client/IQ.h>
#include <xmpp/StanzaHandler.h>

using namespace ::std;

namespace xmpp {
  namespace client {

    namespace { 
      static const char IQ_NAME[] = { "iq" };
      static const char IQ_NAMESPACE[] = { "jabber:client" };
    }

    IQ::IQ() throws()
    {}
    
    IQ::~IQ() throws()
    {}

    const char* IQ::name() const throws()
    { return IQ_NAME; }

    const char* IQ::ns() const throws()
    { return IQ_NAMESPACE; }

    void IQ::write (::std::ostream& out) const
    {
    }

    unique_ptr<StanzaHandler> IQ::createHandler() throws()
    {
      return unique_ptr<StanzaHandler>();
    }
  }
}
