#include <xmpp/client/Message.h>
#include <xmpp/StanzaHandler.h>
#include <cstring>

using namespace ::std;
using namespace ::timber;
using namespace ::xsd;

namespace xmpp {
   namespace client {

      namespace {
         static const char MESSAGE_NAME[] = { "message" };
         static const char MESSAGE_NAMESPACE[] = { "jabber:client" };

         struct MessageHandler : public StanzaHandler
         {
               MessageHandler() throws() :
                  _stanza(new Message())
               {
               }

               ~MessageHandler() throws()
               {
               }

               void notifyStartElement(const char* name, const char* ns, const char* prefix)
               {
                  ++_level;

                  // level = 1; ignore it;
                  if (_level == 1) {
                     return;
                  }
                  else if (_level == 2) {
                     // check which handler we need
                     if (strcmp(name, "body") == 0) {
                        unique_ptr< ElementParser< Message::Body> > p = Message::Body::createParser();
                        p->reset(_stanza->body.add());
                        _handler = move(p);
                     }
                     else if (strcmp(name, "subject") == 0) {
                        unique_ptr< ElementParser< Message::Subject> > p = Message::Subject::createParser();
                        p->reset(_stanza->subject.add());
                        _handler = move(p);
                     }
                     else if (strcmp(name, "thread") == 0) {
                        _stanza->thread.set(Message::Thread());
                        unique_ptr< ElementParser< Message::Thread> > p = Message::Thread::createParser();
                        p->reset(*_stanza->thread);
                        _handler = move(p);
                     }
                     else {
                        throw ::std::runtime_error("Invalid element");
                     }
                  }

                  if (_handler.get() != 0) {
                     _handler->notifyStartElement(name, ns, prefix);
                     return;
                  }
               }

               void notifyElementContent()
               {
                  if (_handler.get() != 0) {
                     _handler->notifyElementContent();
                  }
               }

               void notifyAttribute(const char* name, const char* ns, const char* prefix, const char* value)
               {
                  if (_level == 1) {
                     // message attributes
                  }
                  else if (_handler.get() != 0) {
                     _handler->notifyAttribute(name, ns, prefix, value);
                  }

               }

               void notifyEndElement(const char* name, const char* ns, const char* prefix)
               {
                  if (_handler.get() != 0) {
                     _handler->notifyEndElement(name, ns, prefix);
                  }
                  if (_level == 2) {
                     _handler.reset(0);
                  }
                  --_level;
               }

               void notifyCharacterData(const char* txt, size_t count)
               {
                  if (_handler.get() != 0) {
                     _handler->notifyCharacterData(txt, count);
                  }
                  else {
                     throw ::std::runtime_error("Invalid character data");
                  }
               }

               void notifyIgnorableWhitespace(const char* ws)
               {
                  if (_handler.get() != 0) {
                     _handler->notifyIgnorableWhitespace(ws);
                  }

               }
               void notifyProcessingInstruction(const char* target, const char* data)
               {
                  if (_handler.get() != 0) {
                     _handler->notifyProcessingInstruction(target, data);
                  }
               }

               /** The current xml level */
            private:
               size_t _level;

               /** A vector of stanza read so far */
            private:
               Reference< Message> _stanza;

               /** An element handler */
            private:
               unique_ptr< ::timber::w3c::xml::ElementHandler> _handler;
         };

      }

      Message::Message() throws()
      {
      }

      Message::~Message() throws()
      {
      }

      const char* Message::name() const
      throws()
      {
         return MESSAGE_NAME;
      }

      const char* Message::ns() const
      throws()
      {
         return MESSAGE_NAMESPACE;
      }

      void Message::write(::std::ostream& out) const
      {
      }

      unique_ptr< StanzaHandler> Message::createHandler()
      throws ()
      {
         return unique_ptr< StanzaHandler> ();
      }

   }
}
