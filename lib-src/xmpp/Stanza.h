#ifndef _XMPP_STANZA_H
#define _XMPP_STANZA_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <iosfwd>

namespace xmpp {

  class Stanza : public ::timber::Counted {
  private:
    Stanza(const Stanza&);
    Stanza&operator=(const Stanza&);

    /** Constructor */
  protected:
    Stanza() throws();

    /** Destructor */
  public:
    ~Stanza() throws();

    /**
     * Get the stanza name. This is the simple tag name of the stanza.
     * @return the tag name corresponding to this stanza
     */
  public:
    virtual const char* name() const throws() = 0;

    /**
     * Get the namespace name for this stanza.
     * @return the namespace in which stanza is defined
     */
  public:
    virtual const char* ns() const throws() = 0;

    /**
     * Write this stanza to an output stream.
     * @param out an output stream
     */
  public:
    virtual void write (::std::ostream& out) const = 0;
  };
}
#endif
