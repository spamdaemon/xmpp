#ifndef _XMPP_STANZAHANDLERFACTORY_H
#define _XMPP_STANZAHANDLERFACTORY_H

#ifndef _XMPP_STANZHANDLER_H
#include <xmpp/StanzaHandler.h>
#endif

namespace xmpp {
  
  /**
   * The StanzaHandlerFactory is is able to create instances of DocumentHandlers
   * that can parse a stanza. 
   */
  class StanzaHandlerFactory : public ::timber::Counted {
    StanzaHandlerFactory(const StanzaHandlerFactory&);
    StanzaHandlerFactory&operator=(const StanzaHandlerFactory&);

    /**
     * Constructor
     */
  public:
    StanzaHandlerFactory() throws();

    /**
     * Destructor 
     */
  public:
    virtual ~StanzaHandlerFactory() throws() = 0;

    /**
     * Get a default stanza handler factory. This default factory is able to parse
     * iq, message, and presences stanzas.
     * @return a default factory.
     */
  public:
    static ::timber::Reference<StanzaHandlerFactory> createDefaultFactory() throws();

    /**
     * Get a document handler for the specified stanza.
     * @param name the name of the stanza
     * @param ns the stanza namespace
     * @return a StanzaHandler or a null pointer if it cannot parse the stanza
     */
  public:
    virtual ::std::unique_ptr<StanzaHandler> createHandler (const ::std::string& namme, const ::std::string& ns) const throws() = 0;
  };
}
#endif
