#include <xmpp/SessionPort.h>
#include <xmpp/Stanza.h>
#include <xmpp/StanzaStream.h>
#include <xmpp/StanzaHandlerFactory.h>

#include <timber/net/SocketStreamBuffer.h>
#include <timber/net/server/AbstractSession.h>
#include <timber/logging.h>

#include <iostream>
#include <queue>

using namespace ::std;
using namespace ::timber;
using namespace ::canopy::net;
using namespace ::timber::logging;

namespace xmpp {
   namespace {
      static Log getLog()
      {
         return Log("xmpp.SessionPort");
      }

      static const Long DEFAULT_TIMEOUT_NS = Long(3) * 1000000000;

      struct DataBuffer : public Counted
      {
            DataBuffer() throws() :
               offset(0)
            {
            }

            ~DataBuffer() throws()
            {
            }

            string buffer;
            size_t offset;
      };

      struct SessionImpl : public ::timber::net::server::AbstractSession
      {

            SessionImpl(const Address& extAddr, const Socket& sock, unique_ptr< SessionPort::EventChangeCB> cb,
                  Reference< StanzaHandlerFactory> handlers) throws(exception) :
               ::timber::net::server::AbstractSession(extAddr, sock, move(cb)), _events(READ), _istreambuf(sock),
                     _istream(&_istreambuf), _stream(handlers)
            {
            }

            ~SessionImpl() throws()
            {
            }

            Int events() const throws()
            {
               Int newEvents = READ;
               if (!_outputQueue.empty()) {
                  newEvents |= WRITE;
               }
               return newEvents;
            }

            void read()throws ()
            {
               try {
                  if (_istream && !_istream.eof()) {
                     _istream.read(_buf, sizeof(_buf));
                     if (_istream.gcount() > 0) {
                        // clear any errors
                        LogEntry(getLog()).info() << "Read : " << string(_buf, 0, _istream.gcount()) << doLog;

                        _istream.clear();
                        bool streamOpen = _stream.isOpen();
                        _stream.parseData(_buf, _istream.gcount());

                        Pointer< Stanza> stanza;
                        while ((stanza = _stream.nextStanza())) {
                           getLog().debugging("Read a stanza");
                        }
                        if (!streamOpen && _stream.isOpen()) {
                           enqueue("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                           enqueue(
                                 "<stream:stream from='localhost' id='someid' xmlns=\"jabber:server\" xmlns:stream='http://etherx.jabber.org/streams' version='1.0'>");
                           enqueue("<stream:features/>");
                        }

                        if (_stream.isClosed()) {
                           //FIXME: should probably send a close
                           getLog().info("Stream was closed; closing socket");
                           socket().close();
                        }
                        else {
                           // want to read more data
                           getLog().info("Need more data");
                        }
                        return;
                     }
                     else {
                        // hmmm. There should have been at least 1 byte to read
                        getLog().warn("No data read");
                     }
                  }
                  getLog().info("Closing socket");
                  // should send a close
                  socket().close();
               }
               catch (const exception& e) {
                  getLog().caught("Error reading message", e);
                  // FIXME:
                  // ignore for now
                  socket().close();
               }
            }

            void write()throws()
            {
               // this function is only called, because there is something to write, i.e. we can pull off at least 1 item from the stream
               Reference< DataBuffer> buf = _outputQueue.front();
               size_t n = buf->buffer.length() + buf->offset;
               n = socket().write(buf->buffer.c_str() + buf->offset, n);
               if (n == 0) {
                  //FIXME: check the last time the buffer was attempted to be written
                  // if too long, close the connection
                  getLog().warn("Could not write to socket");
               }
               else {
                  LogEntry(getLog()).info() << "Wrote : " << string(buf->buffer.c_str(), buf->offset, n) << doLog;
                  buf->offset += n;
                  if (buf->offset == buf->buffer.length()) {
                     _outputQueue.pop();
                  }
               }
            }

            void close()throws()
            {
               _events = 0;
            }

            void timeout()throws()
            {
            }

            ::canopy::net::Timeout inactivityTimeout() const throws()
            {
               return ::canopy::net::Timeout::zero();
            }

            void enqueue(const string& data)
            {
               Reference< DataBuffer> buf(new DataBuffer());
               buf->buffer = data;
               _outputQueue.push(buf);
               notifyEventsChanged();
            }

            void enqueue(string& data)
            {
               Reference< DataBuffer> buf(new DataBuffer());
               buf->buffer.swap(data);
               _outputQueue.push(buf);
               notifyEventsChanged();
            }

         private:
            Int _events;

            /** A socket input stream */
         private:
            ::timber::net::SocketStreamBuffer _istreambuf;

            /** A temporary buffer */
         private:
            char _buf[512];

            /** The output buffers */
         private:
            queue< Reference< DataBuffer> > _outputQueue;

            /** The input stream */
         private:
            istream _istream;

            /** The stanza stream */
         private:
            StanzaStream _stream;
      };

      struct PortImpl : public SessionPort
      {
            PortImpl() throws() :
               _stanzaHandlers(StanzaHandlerFactory::createDefaultFactory())
            {
            }

            ~PortImpl() throws()
            {
            }

            Reference< ::timber::net::server::Session> startSession(const Address& extAddr, Socket client, unique_ptr<
                  SessionPort::EventChangeCB> cb)throws (exception)
            {
               return new SessionImpl(extAddr, client, move(cb), _stanzaHandlers);
            }

            /** The stanza handler factory */
         private:
            Reference< StanzaHandlerFactory> _stanzaHandlers;
      };
   }

   SessionPort::SessionPort() throws()
   {
   }

   SessionPort::~SessionPort() throws()
   {
   }

   Reference< SessionPort> SessionPort::create()
   throws()
   {
      return new PortImpl();
   }

}
