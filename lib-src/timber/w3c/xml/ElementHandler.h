#ifndef _TIMBER_W3C_XML_ELEMENTHANDLER_H
#define _TIMBER_W3C_XML_ELEMENTHANDLER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <iosfwd>
#include <memory>

namespace timber {
  namespace w3c {
    namespace xml {

      /**
       * The ElementHandler is simplified version of the document handler. It is only
       * able to parse elements and their content. Comments are assumed to already haveb
       * been removed.
       * @note All text, including element names, attribute
       * names, character data will be passed as UTF-8 encoded ASCII strings.
       */
      class ElementHandler {

	/** Default constructor */
      protected:
	inline ElementHandler() throws () {}

	/**
	 * The destructor
	 */
      public:
	virtual ~ElementHandler() throws () = 0;

	/**
	 * Begin an element. If there are any attributes, then notifyAttribute() follows this
	 * method, otherwise notifyElementContent() will be called immediately.
	 * The namespace will never be 0 and if no namespace is defined for the element, then ns 
	 * is an empty string.
	 * @param name the tag name of the element 
	 * @param ns the namespace URI 
	 * @param prefix the element prefix
	 */
      public:
	virtual void notifyStartElement (const char* name, const char* ns, const char* prefix);
	
 	/**
	 * Add an attribute to the current element. This method is always called after notifyStartElement() and
	 * before notifyElementContent().
	 * The namespace will never be 0 and if no namespace is defined for the attribute, then ns 
	 * is an empty string.
	 * @param name the qualified name of the element
	 * @param ns the namespace uri
	 * @param prefix the attribute name prefix
	 * @param value the attribute value
	 */
      public:
	virtual void notifyAttribute (const char* name, const char* ns, const char* prefix, const char* value);

	/**
	 * Notify this handler that element content is about to be reported. This method is
	 * invoked either after notifyStartElement(), or after the last attribute has been
	 * passed to this handler via notifyAttribute().
	 */
      public:
	virtual void notifyElementContent();

	/**
	 * Notify this class about the start of a CDATA section.
	 */
      public:
	virtual void notifyStartCData ();

	/**
	 * End a CDATA section.
	 */
      public:
	virtual void notifyEndCData ();

	/**
	 * Add parts of a text segment. The byte array passed may not necessarily
	 * start and terminate at a UTF8 codepoint boundary. Only when all character
	 * data is assembled into a single string can the codepoint be extracted.
	 * @param txt a byte array
	 * @param count the number of bytes being passed.
	 */
      public:
	virtual void notifyCharacterData (const char* txt, size_t count);
	
	/**
	 * Add whitespace.
	 */
      public:
	virtual void notifyIgnorableWhitespace(const char* ws);

	/**
	 * Notify this handler that a processing instruction has been parsed.
	 * @param target the target application of the instruction
	 * @param data the application data.
	 */
      public:
	virtual void notifyProcessingInstruction (const char* target, const char* data);

	/**
	 * End an element. 
	 * The namespace will never be 0 and if no namespace is defined for the element, then ns 
	 * is an empty string.
	 * @param name the tag name of the element 
	 * @param ns the namespace URI 
	 * @param prefix the element prefix
	 */
      public:
	virtual void notifyEndElement (const char* name, const char* ns, const char* prefix);
	
      };

    }
  }
}
#endif
