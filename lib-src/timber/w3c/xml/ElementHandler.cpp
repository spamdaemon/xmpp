#include <timber/w3c/xml/ElementHandler.h>

namespace timber {
  namespace w3c {
    namespace xml {
      ElementHandler::~ElementHandler() throws() {}

      void ElementHandler::notifyStartElement (const char* , const char* , const char*)
      {}

      void ElementHandler::notifyElementContent()
      {}

      void ElementHandler::notifyEndElement (const char* , const char* , const char*)
      {}

      void ElementHandler::notifyAttribute (const char* , const char* , const char* , const char*)
      {}

      void ElementHandler::notifyStartCData ()
      {}

      void ElementHandler::notifyEndCData ()
      {}

      void ElementHandler::notifyCharacterData (const char* , size_t )
      {}

      void ElementHandler::notifyIgnorableWhitespace(const char* )
      {}

      void ElementHandler::notifyProcessingInstruction (const char* , const char* )
      {}

    }
  }
}
