#ifndef _XSD_ELEMENT_H
#define _XSD_ELEMENT_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

namespace xsd {

  /**
   * This is a base class for all class defined in this package.
   */
  class Element {

    /** Virtual destructor */
  public:
    virtual ~Element() throws() = 0;
  };
}
#endif

