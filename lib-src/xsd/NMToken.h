#ifndef _XSD_NMTOKEN_H
#define _XSD_NMTOKEN_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _XSD_ELEMENTPARSER_H
#include <xsd/ElementParser.h>
#endif

#include <string>

namespace xsd {

  /**
   * The NMToken is used to represent a string value
   * with a language attribute.
   */
  class NMToken {

    /**
     * Create a new NMToken.
     * @param s the string
     */
  public:
    NMToken() throws();

    /**
     * Create a new NMToken.
     * @param s the string
     */
  public:
    NMToken(const ::std::string& str) throws();

    /**
     * Destructor
     */
  public:
    ~NMToken() throws();


    /**
     * The string value.
     * @return the value of this string
     */
  public:
    const ::std::string& value() const throws()
    { return _value; }

    /**
     * The string value.
     * @return the value of this string
     */
  public:
    ::std::string& value() throws()
      { return _value; }


    /**
     * Get the parser for an Lstring
     * @return a document handler for an NMToken
     * @throws ::std::exception if a handler could not be created
     */
  public:
    static ::std::unique_ptr< ElementParser<NMToken> > createParser() throws (::std::exception);
 
    /** The content */
  private:
    ::std::string _value;
  };
}

#endif
