#ifndef _XSD_OPTIONAL_H
#define _XSD_OPTIONAL_H

#ifndef _XSD_ELEMENT_H
#include <xsd/Element.h>
#endif

namespace xsd {
  
  /**
   * An optional is an object which may have either a single value, or none.
   */
  template <class T> class Optional : public Element {

    /**
     * The copy constructor.
     */
  public:
    Optional(const Optional& opt) throws()
      : Element()
      {
	if (opt._value.get()!=0) {
	  _value.reset(new T(*opt));
	}
      }

    /** constructor */
  public:
    Optional() throws()
      {}

    /** 
     * Create a optional with a single item.
     * @param t an item 
     */
  public:
    Optional(const T& t) throws()
      : _value(new T(t))
      {}

    /** Destructor */
  public:
    ~Optional() throws()
      {}

    /**
     * Copy operator.
     */
  public:
    Optional& operator=(const Optional& opt) throws()
      {
	if (_value.get()!=opt._value.get()) {
	  if (opt._value.get()!=0) {
	    _value.reset(new T(*opt));
	  }
	  else {
	    _value.reset();
	  }
	}
	return *this;
      }

    /**
     * Determine if a value is set.
     * @return true if the value is set, false otherwise
     */
  public:
    inline bool  isSet() const throws() { return _value.get()!=0; }

    /**
     * Returns a pointer to the value. This operator MUST NOT
     * be used if the value is not set.
     * @return a non-null pointer
     */
  public:
    const T* operator->() const throws() { return _value.get(); }

    /**
     * Returns a pointer to the value. This operator MUST NOT
     * be used if the value is not set.
     * @return a non-null pointer
     */
  public:
    T* operator->() throws() { return _value.get(); }

    /**
     * Returns a pointer to the value. This operator MUST NOT
     * be used if the value is not set.
     * @return a non-null pointer
     */
  public:
    const T& operator*() const throws() { return *_value; }

    /**
     * Returns a pointer to the value. This operator MUST NOT
     * be used if the value is not set.
     * @return a non-null pointer
     */
  public:
    T& operator*() throws() { return *_value; }

    /**
     * Set the value. The value will be copied.
     * @param t a value
     */
  public:
    void set (const T& t) throws()
    { 
      _value.reset(new T(t));
    }

    /**
     * Set the value. The value will be copied.
     * @param t a value
     */
  public:
    const T* get () const throws()
    { return _value.get(); }

    /**
     * Set the value. The value will be copied.
     * @param t a value
     */
  public:
    T* get () throws()
    { return _value.get(); }

    /**
     * Set the value. The value will be copied.
     * @param t a value or 0
     */
  public:
    void set (::std::unique_ptr<T> t ) throws()
    { _value = t; }
    
    /**
     * Clear the value
     */
  public:
    void clear() throws()
    { _value.reset(); }

  private:
    ::std::unique_ptr<T> _value;
  };
}
#endif
