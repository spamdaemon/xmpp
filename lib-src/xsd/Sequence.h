#ifndef _XSD_SEQUENCE_H
#define _XSD_SEQUENCE_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _XSD_ELEMENT_H
#include <xsd/Element.h>
#endif

#include <memory>
#include <vector>

namespace xsd {

   /**
    * This class is used to represent an element that contains
    * multiple other object
    */
   template<class T> class Sequence : public Element
   {

         /** constructor */
      public:
         Sequence() throws()
         {
         }

         /**
          * Create a sequence with a single item.
          * @param t an item
          */
      public:
         Sequence(const T& t) throws()
         {
            _values.reserve(1);
            _values.push_back(new T(t));
         }

         /**
          * Create a sequence with a single item.
          * @param t an item pointer
          * @pre t.get()!=0
          */
      public:
         Sequence(::std::unique_ptr< T> t) throws()
         {
            assert(t.get()!=0);
            _values.reserve(1);
            _values.push_back(t);
         }

         /** Destructor */
      public:
         ~Sequence() throws()
         {
            _values.clear();
         }

         /**
          * Get the number of items in this sequence
          * @return the number of items
          */
      public:
         inline size_t size() const throws()
         {
            return _values.size();
         }

         /**
          * Get the value at the specified index.
          * @note The returned reference will only
          * be valid as long as no changes are made to this sequence.
          * @param i an index
          * @return the value at the specified index
          */
      public:
         inline const T& operator[](size_t i) const throws()
         {
            return *_values.at(i);
         }

         /**
          * Get the value at the specified index.
          * @note The returned reference will only
          * be valid as long as no changes are made to this sequence.
          * @param i an index
          * @return the value at the specified index
          */
      public:
         inline T& operator[](size_t i) throws()
         {
            return *_values.at(i);
         }

         /**
          * Add a value to this sequence. This will add an object
          * created with a default constructor and return
          * a reference to that object.
          * <p>
          * The returned referece will ONLY be valid until the next insertion.
          * @return a value to an default constructed object
          */
      public:
         T& add() throws()
         {
            _values.push_back(::std::unique_ptr<T>(new T()));
            return *_values.back();
         }

         /**
          * Add a value to this sequence.
          * @param t the value to be added
          */
      public:
         void add(const T& t) throws()
         {
            _values.push_back(new T(t));
         }

         /**
          * Add a value to this sequence.
          * @param t the value to be added
          * @pre t.get()!=0
          */
      public:
         void add(::std::unique_ptr< T> t) throws()
         {
            assert(t.get()!=0);
            _values.push_back(t);
         }

         /**
          * Remove an object at the specified position.
          * @param i an index
          * @pre i<size()
          */
      public:
         void remove(size_t i) throws()
         {
            delete _values[i];
            _values.erase(_values.begin() + i);
         }

         /** The object */
      private:
         ::std::vector< ::std::unique_ptr< T> > _values;
   };
}
#endif
