#ifndef _XSD_ELEMENTPARSER_H
#define _XSD_ELEMENTPARSER_H

#ifndef _TIMBER_W3C_XML_ELEMENTHANDLER_H
#include <timber/w3c/xml/ElementHandler.h>
#endif

namespace xsd {

  /**
   * The element parser is used to parse an element of a known type.
   */
  template <class T> class ElementParser : public ::timber::w3c::xml::ElementHandler {
    ElementParser(const ElementParser&);
    ElementParser&operator=(const ElementParser&);

    /**
     * Constructor. 
     * Before using this parser, ensure to call reset().
     * 
     */
  protected:
    ElementParser() throws()
      : _element(0)
      {}
    
    /** The destructor */
  public:
    ~ElementParser() throws() {}

    /**
     * Reset this parser.
     * @param element the element into which to parse
     */
  public:
    void reset (T& t) throws()
    { _element = &t; }
    
    /** The element to be populated by this parse */
  protected:
    inline T& element () throws()
    { return *_element; }

  private:
    T* _element;
  };
}

#endif
