#include <xsd/NMToken.h>
#include <cstring>

using namespace ::std;

namespace xsd {
  
  namespace {
      
    struct Handler : public xsd::ElementParser<NMToken> {
      Handler() 
	: _level(0) {}
      ~Handler() throws() {}

      void notifyStartElement (const char* name, const char* ns, const char* prefix)
      {
	++_level;
	  
	// level=1: root tag
	if (_level==1) {
	  element() = NMToken();
	}
	else {
	  throw ::std::runtime_error(string("Invalid child tag {")+ns+"}"+name);
	}
      }
	
      void notifyAttribute (const char* name, const char* ns, const char* prefix, const char* value)
      {
	throw ::std::runtime_error(string("Invalid attribute {")+ns+"}"+name);
      }
	  
      void notifyEndElement(const char* name, const char* ns, const char* prefix)
      {
	--_level;
      }
	
      void notifyCharacterData (const char* txt, size_t count)
      {
	element().value().append(txt,count);
      }

    private:
      size_t _level;
    };
  }
  
  NMToken::NMToken() throws()
  {}
  
  NMToken::NMToken(const ::std::string& str) throws()
  : _value(str)
  {}
  
  NMToken::~NMToken() throws()
  {}
  
  ::std::unique_ptr< ElementParser<NMToken> > NMToken::createParser() throws (::std::exception)
  {
    return unique_ptr<ElementParser<NMToken> >(new Handler());
  }
}
