#include <timber/net/server/Server.h>
#include <timber/net/server/SessionPort.h>
#include <xmpp/SessionPort.h>

#include <iostream>

using namespace ::std;
using namespace ::canopy::net;
using namespace ::timber;
using namespace ::timber::net::server;

int main()
{
  try {
    Socket socket = Socket::createBoundSocket(Endpoint::createAnyEndpoint(11443,STREAM));
    socket.listen(1);

    Reference<SessionPort> port = ::xmpp::SessionPort::create();
    unique_ptr<Server> server = Server::create(0);
    server->addSessionPort(socket,port);
    
    server->wait();

    return 0;
  }
  catch (const exception& e) {
    ::std::cerr << "Exception " << e.what() << ::std::endl;
    return 1;
  }
}
